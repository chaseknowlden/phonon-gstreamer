# Translation of phonon_gstreamer_qt to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2009, 2011.
msgid ""
msgstr ""
"Project-Id-Version: KDE 4\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2019-04-13 01:47+0200\n"
"PO-Revision-Date: 2011-08-10 19:32+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <l10n-no@lister.huftis.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: qtrich\n"
"X-Generator: Lokalize 1.1\n"
"X-Qt-Contexts: true\n"

#: gstreamer/backend.cpp:220
msgctxt "Phonon::Gstreamer::Backend|"
msgid ""
"Warning: You do not seem to have the package gstreamer1.0-plugins-good "
"installed.\n"
"          Some video features have been disabled."
msgstr ""

#: gstreamer/backend.cpp:228
msgctxt "Phonon::Gstreamer::Backend|"
msgid ""
"Warning: You do not seem to have the base GStreamer plugins installed.\n"
"          All audio and video support has been disabled"
msgstr ""

#: gstreamer/mediaobject.cpp:428
msgctxt "Phonon::Gstreamer::MediaObject|"
msgid "Default"
msgstr ""

#: gstreamer/mediaobject.cpp:442 gstreamer/mediaobject.cpp:475
msgctxt "Phonon::Gstreamer::MediaObject|"
msgid "Unknown"
msgstr ""

#: gstreamer/mediaobject.cpp:462
msgctxt "Phonon::Gstreamer::MediaObject|"
msgid "Disable"
msgstr ""

#: gstreamer/pipeline.cpp:515
msgctxt "Phonon::Gstreamer::Pipeline|"
msgid "One or more plugins are missing in your GStreamer installation."
msgstr ""

#: gstreamer/plugininstaller.cpp:188
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "Missing codec helper script assistant."
msgstr ""

#: gstreamer/plugininstaller.cpp:190
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "Plugin codec installation failed."
msgstr ""

#: gstreamer/plugininstaller.cpp:214
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "Phonon attempted to install an invalid codec name."
msgstr ""

#: gstreamer/plugininstaller.cpp:217
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "The codec installer crashed."
msgstr ""

#: gstreamer/plugininstaller.cpp:220
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "The required codec could not be found for installation."
msgstr ""

#: gstreamer/plugininstaller.cpp:223
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "An unspecified error occurred during codec installation."
msgstr ""

#: gstreamer/plugininstaller.cpp:226
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "Not all codecs could be installed."
msgstr ""

#: gstreamer/plugininstaller.cpp:229
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "User aborted codec installation"
msgstr ""

#: gstreamer/plugininstaller.cpp:240
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "Could not update plugin registry after update."
msgstr ""
