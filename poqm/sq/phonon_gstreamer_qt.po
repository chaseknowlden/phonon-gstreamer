# Albanian translation for phonon
# Copyright (c) 2009 Rosetta Contributors and Canonical Ltd 2009
# This file is distributed under the same license as the phonon package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: phonon\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-07-01 01:43+0200\n"
"PO-Revision-Date: 2009-07-20 05:58+0000\n"
"Last-Translator: Vilson Gjeci <vilsongjeci@gmail.com>\n"
"Language-Team: Albanian <sq@li.org>\n"
"Language: sq\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2011-05-05 22:19+0000\n"
"X-Generator: Launchpad (build 12959)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Qt-Contexts: true\n"

#: gstreamer/backend.cpp:220
#, fuzzy
#| msgid ""
#| "Warning: You do not seem to have the package gstreamer0.10-plugins-good "
#| "installed.\n"
#| "          Some video features have been disabled."
msgctxt "Phonon::Gstreamer::Backend|"
msgid ""
"Warning: You do not seem to have the package gstreamer1.0-plugins-good "
"installed.\n"
"          Some video features have been disabled."
msgstr ""
"Warning: You do not seem to have the package gstreamer0.10-plugins-good "
"installed.\n"
"          Some video features have been disabled."

#: gstreamer/backend.cpp:228
#, fuzzy
#| msgid ""
#| "Warning: You do not seem to have the base GStreamer plugins installed.\n"
#| "          All audio and video support has been disabled"
msgctxt "Phonon::Gstreamer::Backend|"
msgid ""
"Warning: You do not seem to have the base GStreamer plugins installed.\n"
"          All audio and video support has been disabled"
msgstr ""
"Warning: You do not seem to have the base GStreamer plugins installed.\n"
"          All audio and video support has been disabled"

#: gstreamer/mediaobject.cpp:428
msgctxt "Phonon::Gstreamer::MediaObject|"
msgid "Default"
msgstr ""

#: gstreamer/mediaobject.cpp:442 gstreamer/mediaobject.cpp:475
msgctxt "Phonon::Gstreamer::MediaObject|"
msgid "Unknown"
msgstr ""

#: gstreamer/mediaobject.cpp:462
msgctxt "Phonon::Gstreamer::MediaObject|"
msgid "Disable"
msgstr ""

#: gstreamer/pipeline.cpp:515
msgctxt "Phonon::Gstreamer::Pipeline|"
msgid "One or more plugins are missing in your GStreamer installation."
msgstr ""

#: gstreamer/plugininstaller.cpp:188
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "Missing codec helper script assistant."
msgstr ""

#: gstreamer/plugininstaller.cpp:190
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "Plugin codec installation failed."
msgstr ""

#: gstreamer/plugininstaller.cpp:214
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "Phonon attempted to install an invalid codec name."
msgstr ""

#: gstreamer/plugininstaller.cpp:217
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "The codec installer crashed."
msgstr ""

#: gstreamer/plugininstaller.cpp:220
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "The required codec could not be found for installation."
msgstr ""

#: gstreamer/plugininstaller.cpp:223
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "An unspecified error occurred during codec installation."
msgstr ""

#: gstreamer/plugininstaller.cpp:226
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "Not all codecs could be installed."
msgstr ""

#: gstreamer/plugininstaller.cpp:229
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "User aborted codec installation"
msgstr ""

#: gstreamer/plugininstaller.cpp:240
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "Could not update plugin registry after update."
msgstr ""
